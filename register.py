class Register(object):

    def __init__(self):
        self.value = 0
        self.flag = 0

    def add(self,y):
        self.value = self.value + y
        if self.value>0xFF:
            self.value %= 256
            self.flag = 1
        else:
            self.flag = 0

    def sub(self, y):
        self.value -= y
        if self.value<0x00:
            self.value %= 256
            self.flag = 1
        else:
            self.flag = 0


