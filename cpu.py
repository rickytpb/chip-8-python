import inspect
import random
import sys
import os
import time
import pygame
from register import Register
from screen import Screen



class CPU(object):

    DELAY = 0.002

    KEY_MAPPINGS = {
        pygame.K_KP0: 0x0,
        pygame.K_UP: 0x1,
        pygame.K_KP2: 0x2,
        pygame.K_KP3: 0x3,
        pygame.K_DOWN: 0x4,
        pygame.K_KP5: 0x5,
        pygame.K_KP6: 0x6,
        pygame.K_KP7: 0x7,
        pygame.K_KP8: 0x8,
        pygame.K_KP9: 0x9,
        pygame.K_a: 0xA,
        pygame.K_b: 0xB,
        pygame.K_w: 0xC,
        pygame.K_s: 0xD,
        pygame.K_e: 0xE,
        pygame.K_f: 0xF,
    }

    def __init__(self):
        # Memory
        self.memory = [0x0 for i in range(4096)]

        # Registers
        self.v = [Register() for i in range(0x10)]
        self.delayt = 0x0
        self.soundt = 0x0
        self.i = 0
        self.stack = []
        self.pc = 0x200
        self.screenx = 64
        self.screeny = 32

        #SCREEN
        pygame.init()
        self.screen = Screen()

        # Current operation
        self.op = None

        # Operations lookup
        self.op_code = {
            0x0: self.clear_return,
            0x1: self.jump_to_address,
            0x2: self.subroutine,
            0x3: self.skip_if_equal,
            0x4: self.skip_if_not_equal,
            0x5: self.skip_if_vx_is_vy,
            0x6: self.store_in_vx,
            0x7: self.add_to_vx,
            0x8: self.operate_vx_vy,
            0x9: self.skip_if_vx_not_vy,
            0xA: self.store_addr_in_i,
            0xB: self.jump_toaddr_vy,
            0xC: self.set_vx_random_with_mask,
            0xD: self.draw_sprite,
            0xE: self.skip_if_key,
            0xF: self.vx_operations
        }

        #FONTS
        self.fonts = [0xF0, 0x90, 0x90, 0x90, 0xF0,  # 0
                 0x20, 0x60, 0x20, 0x20, 0x70,  # 1
                 0xF0, 0x10, 0xF0, 0x80, 0xF0,  # 2
                 0xF0, 0x10, 0xF0, 0x10, 0xF0,  # 3
                 0x90, 0x90, 0xF0, 0x10, 0x10,  # 4
                 0xF0, 0x80, 0xF0, 0x10, 0xF0,  # 5
                 0xF0, 0x80, 0xF0, 0x90, 0xF0,  # 6
                 0xF0, 0x10, 0x20, 0x40, 0x40,  # 7
                 0xF0, 0x90, 0xF0, 0x90, 0xF0,  # 8
                 0xF0, 0x90, 0xF0, 0x10, 0xF0,  # 9
                 0xF0, 0x90, 0xF0, 0x90, 0x90,  # A
                 0xE0, 0x90, 0xE0, 0x90, 0xE0,  # B
                 0xF0, 0x80, 0x80, 0x80, 0xF0,  # C
                 0xE0, 0x90, 0x90, 0x90, 0xE0,  # D
                 0xF0, 0x80, 0xF0, 0x80, 0xF0,  # E
                 0xF0, 0x80, 0xF0, 0x80, 0x80  # F
                 ]

        self.font_start = 0x50
        self.font_size = 0x5
        for i in range(len(self.fonts)):
            self.memory[self.font_start+i] = self.fonts[i]

        self.matrix = [[0 for i in range(64)] for i in range(32)]

    def main_loop(self):
        self.op = 0xFF
        pygame.mixer.init()
        sound = pygame.mixer.Sound("click.wav")
        while self.op != 0x0:
            self.op = self.read_opcode()
            lead = (self.op & 0xF000) >> 12
            print(self.op_code[lead])
            print(hex(self.op))
            try:
                self.op_code[lead]()
            except KeyError:
                print("INVALID OPERATION")
                sys.exit(0)

            if self.delayt > 0:
                self.delayt -= 1
            if self.soundt > 0:
                if self.soundt == 1:
                    sound.play()
                self.soundt -= 1

            #PYGAME

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return
            #self.screen.draw_screen(self.matrix)

            time.sleep(self.DELAY)



    def draw_screen(self):
        os.system('cls')
        for p in range(32):
            for i in range(64):
                sys.stdout.write(str(self.matrix[p][i]))
            print("")
    
    def incr_pc(self):
        self.pc += 0x2

    def read_opcode(self):
        lop = self.memory[self.pc]
        rop = self.memory[self.pc+0x1]
        opcode = 256 * lop + rop
        return opcode

    def load_rom(self, file):
        with open(file, 'rb') as file:
            data = file.read()
            size = file.tell()
            start = 0x200

            for i in range(size):
                self.memory[start + i] = data[i]

    def print_mem(self):
        for i in range(0xFFF):
            print(str(self.memory[i]) + 'address: ' + str(hex(i)))

    def skip(self):
        self.pc += 0x4

    def clear_return(self):
        #DEFINETLY OK
        #print(inspect.stack()[0][3])
        #Clears the screen
        if self.op == 0x00E0:
            for p in range(32):
                for i in range(64):
                    self.matrix[p][i] = 0
            self.incr_pc()
        #Returns from a subroutine
        if self.op == 0x00EE:
            self.pc = self.stack.pop()

    def jump_to_address(self):
        #DEFINETLY OK
        #Jumps to address NNN
        #print(inspect.stack()[0][3])
        addr = self.op & 0x0FFF
        self.pc = addr

    def subroutine(self):
        #DEFINETLY OK
        #Calls a subroutine
        self.incr_pc()
        self.stack.append(self.pc)
        self.pc = (self.op & 0x0FFF)
        #print(inspect.stack()[0][3])

    def skip_if_equal(self):
        #DEFINETLY OK
        #Skips next instr if vx==nn
        x = (self.op & 0x0F00) >> 8
        nn = (self.op & 0x00FF)
        #print(inspect.stack()[0][3])
        if self.v[x].value == nn:
            self.skip()
        else:
            self.incr_pc()


    def skip_if_not_equal(self):
        #DEFINETLY OK
        #Skips next instr if vx!=nn
        #print(inspect.stack()[0][3])
        x = (self.op & 0x0F00) >> 8
        nn = (self.op & 0x00FF)
        if self.v[x].value != nn:
            self.skip()
        else:
            self.incr_pc()

    def skip_if_vx_not_vy(self):
        #DEFINETLY OK
        #Skips next instr if vx!=vy
        #print(inspect.stack()[0][3])
        x = (self.op & 0x0F00) >> 8
        y = (self.op & 0x00F0) >> 4
        if self.v[x].value != self.v[y].value:
            self.skip()
        else:
            self.incr_pc()

    def store_in_vx(self):
        #DEFINETLY OK
        #Stores nn in vx
        #print(inspect.stack()[0][3])
        x = (self.op & 0x0F00) >> 8
        nn = (self.op & 0x00FF)
        self.v[x].value = nn
        self.incr_pc()

    def add_to_vx(self):
        #DEFINETLY OK
        #Adds nn to vx
        x = (self.op & 0x0F00) >> 8
        nn = (self.op & 0x00FF)
        #print(inspect.stack()[0][3])
        self.v[x].add(nn)
        self.incr_pc()

    def operate_vx_vy(self):
        #SHOULD BE OK
        #Various vx, vy operations
        #print(inspect.stack()[0][3])
        x = (self.op & 0x0F00) >> 8
        y = (self.op & 0x00F0) >> 4
        last_n = self.op & 0x000F
        if last_n == 0x0:
            self.v[x].value = self.v[y].value
        if last_n == 0x1:
            self.v[x].value = self.v[x].value | self.v[y].value
        if last_n == 0x2:
            self.v[x].value = self.v[x].value & self.v[y].value
        if last_n == 0x3:
            self.v[x].value = self.v[x].value ^ self.v[y].value
        if last_n == 0x4:
            self.v[x].add(self.v[y].value)
            overflow = self.v[x].flag
            if overflow:
                self.v[0xF].value = 1
            else:
                self.v[0xF].value = 0
        if last_n == 0x5:
            self.v[x].sub(self.v[y].value)
            underflow = self.v[x].flag
            if underflow:
                self.v[0xF].value = 0
            else:
                self.v[0xF].value = 1

        if last_n == 0x6:
            self.v[x].value = self.v[x].value >> 1
            self.v[0xf].value = self.v[x].value & 1

        if last_n == 0x7:
            value = self.v[y].value - self.v[x].value
            self.v[x].value = value % 256
            if value< 0:
                self.v[0xf].value = 0
            else:
                self.v[0xf].value = 1

        if last_n == 0xE:
            self.v[x].value = self.v[x].value << 1
            self.v[0xf].value  = self.v[x].value >> 7

        self.incr_pc()


    def skip_if_vx_is_vy(self):
        #DEFINETLY OK
        ##print(inspect.stack()[0][3])
        x = (self.op & 0x0F00) >> 8
        y = (self.op & 0x00F0) >> 4
        if self.v[x].value == self.v[y].value:
            self.skip()
        else:
            self.incr_pc()

    def store_addr_in_i(self):
        #DEFINETLY OK
        #print(inspect.stack()[0][3])
        addr = self.op & 0x0FFF
        self.i = addr
        self.incr_pc()

    def jump_toaddr_vy(self):
        input("JUMP TO ADDR + V0")
        #print(inspect.stack()[0][3])
        addr = (self.op & 0x0FFF) + self.v[0x0].value
        self.pc = addr

    def set_vx_random_with_mask(self):
        #DEFINETLY OK
        #print(inspect.stack()[0][3])
        x = (self.op & 0x0F00) >> 8
        nn = (self.op & 0x00FF)
        rand = random.randint(0, 0xFF)
        self.v[x].value = (rand & nn)
        self.incr_pc()

    def draw_sprite(self):
        #DEFINETLY OK
        x = (self.op & 0x0F00) >> 8
        y = (self.op & 0x00F0) >> 4
        x_pos = self.v[x].value
        y_pos = self.v[y].value
        n = (self.op & 0x000F)
        self.v[0xF].value = 0x0
        for i in range(n):
            self.byte = '{0:08b}'.format(self.memory[self.i + i])
            y_c = y_pos + i
            y_c = y_c % self.screeny
            for j in range(8):
                x_c = x_pos + j
                x_c = x_c % self.screenx

                new_val = int(self.byte[j])
                old_val = self.matrix[y_c][x_c]
                if new_val == 1 and old_val == 1:
                    self.v[0xF].value = self.v[0xF].value | 1
                    new_val = 0
                elif new_val == 0 and old_val == 1:
                    new_val = 1

                self.matrix[y_c][x_c] = new_val

        self.screen.draw_screen(self.matrix)
        self.incr_pc()
        #print(inspect.stack()[0][3])

    def skip_if_key(self):
        #input("SKIP IF KEY")
        #print(inspect.stack()[0][3])
        x = (self.op & 0x0F00) >> 8
        op = self.op & 0x00FF
        keys_pressed = pygame.key.get_pressed()

        if op == 0x9e:
            key_code = None
            key = self.v[x].value
            for hex,val in self.KEY_MAPPINGS.items():
                if key == val:
                    key_code = hex
            if keys_pressed[key_code] == 1:
                self.skip()
            else:
                self.incr_pc()

        if op == 0xa1:
            key_code = None
            key = self.v[x].value
            for hex,val in self.KEY_MAPPINGS.items():
                if key == val:
                    key_code = hex
            if keys_pressed[key_code] == 0:
                self.skip()
            else:
                self.incr_pc()


    def vx_operations(self):
        #input("VX")
        
        x = (self.op & 0x0F00) >> 8
        end = self.op & 0x00FF

        if end == 0x07:
            self.v[x].value = self.delayt
            self.incr_pc()

        if end == 0x0A:
            key_pressed = False
            while not key_pressed:
                events = pygame.event.get()
                for event in events:
                    if event.type == pygame.KEYDOWN:
                        if event.key in self.KEY_MAPPINGS:
                            self.v[x].value = (self.KEY_MAPPINGS[event.key])
                            key_pressed = True
            self.incr_pc()

        #OK
        if end == 0x15:
            x = (self.op & 0x0F00) >> 8
            self.delayt = self.v[x].value
            self.incr_pc()

        #OK
        if end == 0x18:
            x = (self.op & 0x0F00) >> 8
            self.soundt = self.v[x].value
            self.incr_pc()

        #OK
        if end == 0x1e:
            x = (self.op & 0x0F00) >> 8
            self.i += self.v[x].value
            self.incr_pc()

        #OK
        if end == 0x29:
            x = (self.op & 0x0F00) >> 8
            self.i = self.font_start + self.v[x].value * 0x5
            self.incr_pc()

        #OK
        if end == 0x33:
            number = str(self.v[x].value)
            number = number.zfill(4)
            ones = int(number[-1])
            tens = int(number[-2])
            hundreds = int(number[-3])
            self.memory[self.i] = hundreds
            self.memory[self.i + 1] = tens
            self.memory[self.i + 2] = ones
            self.incr_pc()

        if end == 0x55:
            #Stores V0 to VX (including VX) in memory starting at address I.
            x = (self.op & 0x0F00) >> 8
            for i in range(x+1):
                self.memory[self.i+i] = self.v[i].value
            # Original interpreter does this
            #self.i += x + 1
            self.incr_pc()

        #OK
        if end == 0x65:
            x = (self.op & 0x0F00) >> 8
            for i in range(x+1):
                self.v[i].value = self.memory[self.i+i]
            #Original interpreter does this
            #self.i += x + 1
            self.incr_pc()


if __name__ == '__main__':
    cpu = CPU()
    cpu.load_rom('PONG')
    cpu.main_loop()
