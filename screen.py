import pygame

class Screen(object):

    def __init__(self):
        self.pixsize = 12
        self.screen = [[0 for i in range(64)] for i in range(32)]
        self.screen = pygame.display.set_mode((self.pixsize * 64 ,self.pixsize * 32))
        self.screen.set_alpha(None)
        self.background = pygame.Surface(self.screen.get_size())
        self.background = self.background.convert()
        self.background.fill((0, 0, 0))

    def draw_screen(self, matrix):
        self.screen.blit(self.background, (0,0))
        self.drawrects(matrix)
        pygame.display.update()
        pygame.display.flip()

    def drawrects(self, matrix):
        for x in range(32):
            for y in range(64):
                rect = pygame.Rect(y*self.pixsize,x*self.pixsize, self.pixsize, self.pixsize)
                if matrix[x][y] == 0:
                    pass
                    #pygame.draw.rect(self.screen,(255,255,255),rect,0)
                else:
                    pygame.draw.rect(self.screen, (255, 255, 255), rect, 0)

    def main(self):
        while 1:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return
            self.draw_screen()

if __name__ == '__main__':
    scr = Screen()
    scr.main()